var config = require('./helpers/getConfig.js');

var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var jpegoptim = require('imagemin-jpegoptim');
var pngquant = require('imagemin-pngquant');


gulp.task('imagemin', function() {
	var stream = gulp.src([
		'**/*.{png,jpg,gif,svg}',
		'!bg/icons-svg.svg',
	], {
		cwd: config.dest.images,
	});

	stream
		.pipe(imagemin([
			jpegoptim({
				progressive: true,
			}),
			pngquant(),
			imagemin.gifsicle(),
			imagemin.svgo(),
		]))
		.pipe(gulp.dest(config.dest.images));

	return stream;
});
