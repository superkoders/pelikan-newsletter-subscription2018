var config = require('./helpers/getConfig.js');

var gulp = require('gulp');
var stylint = require('gulp-stylint');


gulp.task('stylint', function () {
	var stream = gulp.src([
		'**/*.styl',
	], {
		cwd: config.src.styles,
	});

	stream
		.pipe(stylint({
			config: '.stylintrc',
			reporter: 'stylint-stylish',
			reporterOptions: {
				verbose: true,
				absolutePath: true,
			},
		}))
		.pipe(stylint.reporter());

	return stream;
});
