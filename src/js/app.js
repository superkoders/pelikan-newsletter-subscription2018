import $ from 'jquery';
// import './tools/svg4everybody'; // TODO - causing issue in chrome?
// import { MQ, showDevices } from './tools/MQ';

// Components
// import * as fontFaceObserver from './components/fontFaceObserver';
import * as whatInput from './components/whatInput';
import * as canTouch from './components/canTouch';
// import * as preventClick from './components/preventClick';

// content load components
const componentsload = [
];

// once delegated components
const components = [
	whatInput,
	canTouch,
	// preventClick,
].concat( componentsload );

window.App = {
	run() {
		var $target = $(document);
		components.forEach((component) => component.init( $target ));

		$(document)
			.on('contentload', function(event) {
				var $target = $(event.target);
				componentsload.forEach((component) => component.init( $target ));
			});
	},

	initComponent(component) {
		return component.init();
	},
};
