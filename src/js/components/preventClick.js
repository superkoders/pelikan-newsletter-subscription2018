import $ from 'jquery';

export const init = () => {
	function documentClickListener(event) {
		var $target = $(event.target);

		if ($target.closest('[data-prevent-click]:not([data-prevent-click="false"])').length > 0) {
			event.preventDefault();
			event.stopPropagation();
		}
	}

	document.addEventListener('click', documentClickListener, true);
};
