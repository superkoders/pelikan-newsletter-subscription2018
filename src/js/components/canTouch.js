// Based on method #4 here: http://www.javascriptkit.com/dhtmltutors/sticky-hover-issue-solutions.shtml
// Original demo: http://editor.javascriptkit.com/?eg=dynamic-css-hover

export const init = () => {
	let isTouch = false;
	let timeoutDelay = 500;
	let isTouchTimer;
	let canTouch = false;

	function addtouchclass() {
		clearTimeout(isTouchTimer);
		isTouch = true;
		if (!canTouch) {
			canTouch = true;
			document.documentElement.setAttribute('data-cantouch', true);
		}
		isTouchTimer = setTimeout(() => isTouch = false, timeoutDelay);
	}

	function removetouchclass() {
		if (!isTouch && canTouch) {
			isTouch = false;
			canTouch = false;
			document.documentElement.setAttribute('data-cantouch', false);
		}
	}

	document.documentElement.setAttribute('data-cantouch', false);
	document.addEventListener('touchstart', addtouchclass, false);
	document.addEventListener('mouseover', removetouchclass, false);
};
